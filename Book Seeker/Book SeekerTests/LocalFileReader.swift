//
//  LocalFileReader.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

final class LocalFileReader {
    static func readJSONFile(name: String) -> Data {
        let bundle = Bundle(for: LocalFileReader.self)
        let path = bundle.path(forResource: name, ofType: "json")
        let data = try! Data(contentsOf: URL(fileURLWithPath: path!), options: .mappedIfSafe)
        return data
    }
}
