//
//  RequestBuilderTests.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import XCTest
@testable import Book_Seeker

final class RequestBuilderTests: XCTestCase {

    // MARK: Tests

    func test_requestBuilder_returnsCorrectURl() {
        let endPoint = APIEndpointDummy(path: "/search",
                                        scheme: "https",
                                        host: "itunes.apple.com",
                                        headers: .default,
                                        httpMethod: .get,
                                        queryStringArguments: [
                                            QueryStringRequesterArgument(key: "key", value: "value"),
                                            QueryStringRequesterArgument(key: "otherKey", value: "otherValue")
        ])
        let expectedURLRequest = URLRequest(url: URL(string: "https://itunes.apple.com/search?key=value&otherKey=otherValue")!)
        let urlRequest = RequestBuilder.buildRequest(from: endPoint)
        XCTAssertEqual(urlRequest, expectedURLRequest)
    }
}
