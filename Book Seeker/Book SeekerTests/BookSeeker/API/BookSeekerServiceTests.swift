//
//  BookSeekerServiceTests.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import XCTest
@testable import Book_Seeker

final class BookSeekerServiceTests: XCTestCase {

    // MARK: Properties

    private var requesterDummy: APIRequesterProtocolDummy!
    private var service: BookSeekerService!

    // MARK: Life cycle

    override func setUp() {
        super.setUp()
        requesterDummy = APIRequesterProtocolDummy()
        service = BookSeekerService(requester: requesterDummy)
    }

    override func tearDown() {
        super.tearDown()
        requesterDummy = nil
        service = nil
    }

    // MARK: Tests

    func test_searchForValidBook_returnsListOfBookSeekerAPIResponse() {
        let asyncExpectation = expectation(description: #function)
        let data = LocalFileReader.readJSONFile(name: "BookSearchSuccessJSON")
        requesterDummy.requestResult = .success(data)

        service.searchForBook("some name") { result in
            switch result {
            case .success(let list):
                XCTAssertEqual(list.count, 50)
            case .failure:
                XCTFail("Shouldnt have failed here")
            }

            asyncExpectation.fulfill()
        }

        wait(for: [asyncExpectation], timeout: 1)
    }

    func test_searchForBook_givenUnexpectedJson_returnsDecodeError() {
        let asyncExpectation = expectation(description: #function)
        let data = LocalFileReader.readJSONFile(name: "BookSearchFailJSON")
        requesterDummy.requestResult = .success(data)

        service.searchForBook("some name") { result in
            switch result {
            case .success:
                XCTFail("Shouldnt have failed here")
            case .failure(let error):
                let castError = error as? APIDecoderError
                XCTAssertNotNil(castError)
                XCTAssertEqual(castError, .decode)
            }

            asyncExpectation.fulfill()
        }

        wait(for: [asyncExpectation], timeout: 1)
    }
}
