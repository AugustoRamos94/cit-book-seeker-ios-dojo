//
//  BookSeekerViewModelTests.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import XCTest
@testable import Book_Seeker

final class BookSeekerViewModelTests: XCTestCase {

    // MARK: Properties

    private var viewModel: BookSeekerViewModel!
    private var bookServiceDummy: BookSeekerServiceDummy!

    // MARK: Life cycle

    override func setUp() {
        super.setUp()
        bookServiceDummy = BookSeekerServiceDummy()
        viewModel = BookSeekerViewModel(bookService: bookServiceDummy)
    }

    override func tearDown() {
        super.tearDown()
        bookServiceDummy = nil
        viewModel = nil
    }

    // MARK: Tests

    func test_searchForABook_triggersOnPerformingAsyncRequest() {
        let asyncExpectation = expectation(description: #function)
        asyncExpectation.expectedFulfillmentCount = 2

        viewModel.onPerformingAsyncRequest = { _ in
            asyncExpectation.fulfill()
        }

        viewModel.searchForBook("some book")
        wait(for: [asyncExpectation], timeout: 1)
    }

    func test_searchForABookSuccess_triggersOnListHasChanged() {
        let asyncExpectation = expectation(description: #function)

        let result: [Book] = [
            Book(trackId: 0, name: "name", description: "description", authorName: "authorName", url: "url", releaseDate: "releaseDate"),
            Book(trackId: 1, name: "name", description: "description", authorName: "authorName", url: "url", releaseDate: "releaseDate")
        ]
        bookServiceDummy.searchForBookResult = .success(result)
        viewModel.onListHasChanged = {
            asyncExpectation.fulfill()
        }

        viewModel.searchForBook("some book")
        wait(for: [asyncExpectation], timeout: 1)
    }

    func test_viewDidSelectItem_triggersOnSelectItem() {
        let asyncExpectation = expectation(description: #function)

        let result: [Book] = [
            Book(trackId: 0, name: "name", description: "description", authorName: "authorName", url: "url", releaseDate: "releaseDate"),
            Book(trackId: 1, name: "name", description: "description", authorName: "authorName", url: "url", releaseDate: "releaseDate")
        ]
        bookServiceDummy.searchForBookResult = .success(result)
        viewModel.onSelectItem = { item in
            XCTAssertEqual(item, result.first!)
            asyncExpectation.fulfill()
        }

        viewModel.searchForBook("some book")
        viewModel.viewDidSelectItem(atIndexPath: IndexPath(item: 0, section: 0))
        wait(for: [asyncExpectation], timeout: 1)
    }

    func test_searchForABookFail_triggersOnRequestError() {
        let asyncExpectation = expectation(description: #function)

        bookServiceDummy.searchForBookResult = .failure(APIRequesterError.invalidURL)
        viewModel.onRequestError = {
            asyncExpectation.fulfill()
        }

        viewModel.searchForBook("some book")
        wait(for: [asyncExpectation], timeout: 1)
    }

    func test_searchForABook_giveCorrectNumberOfBooks() {
        let result: [Book] = [
            Book(trackId: 0, name: "name", description: "description", authorName: "authorName", url: "url", releaseDate: "releaseDate"),
            Book(trackId: 1, name: "name", description: "description", authorName: "authorName", url: "url", releaseDate: "releaseDate")
        ]
        bookServiceDummy.searchForBookResult = .success(result)
        viewModel.searchForBook("some book")
        XCTAssertEqual(viewModel.numberOfItemsInSection(0), result.count)
    }
}
