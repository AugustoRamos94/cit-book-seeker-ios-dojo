//
//  BookSeekerCoordinatorTests.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import XCTest
@testable import Book_Seeker

final class BookSeekerCoordinatorTests: XCTestCase {

    // MARK: Properties

    private var navigationController: UINavigationController!
    private var coordinator: BookSeekerCoordinator!

    // MARK: Life cycle

    override func setUp() {
        super.setUp()
        navigationController = UINavigationController()
        coordinator = BookSeekerCoordinator(navigationController: navigationController)
    }

    override func tearDown() {
        super.tearDown()
        navigationController = nil
        coordinator = nil
    }

    // MARK: Tests

    func test_startCoordinator_willHaveCorrectController() {
        XCTAssertEqual(navigationController.viewControllers.count, 0)
        coordinator.start()
        
        let controller = navigationController.viewControllers.first as? BookSeekerViewController
        XCTAssertNotNil(controller)
    }
}
