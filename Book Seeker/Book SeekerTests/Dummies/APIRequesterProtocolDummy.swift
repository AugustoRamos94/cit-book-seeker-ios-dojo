//
//  APIRequesterProtocolDummy.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation
@testable import Book_Seeker

final class APIRequesterProtocolDummy: APIRequesterProtocol {
    var requestResult: Result<Data, Error> = .success(Data())
    func request(endPoint: APIEndpointProtocol, completion: @escaping RequestResult) {
        completion(requestResult)
    }
}
