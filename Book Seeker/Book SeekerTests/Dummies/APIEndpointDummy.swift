//
//  APIEndpointDummy.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation
@testable import Book_Seeker

struct APIEndpointDummy: APIEndpointProtocol {
    let path: String
    let scheme: String
    let host: String
    let headers: HTTPHeaders?
    let httpMethod: HTTPMethod
    let queryStringArguments: [QueryStringRequesterArgument]
}
