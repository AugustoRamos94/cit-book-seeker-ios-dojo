//
//  BookSeekerServiceDummy.swift
//  Book SeekerTests
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation
@testable import Book_Seeker

final class BookSeekerServiceDummy: BookSeekerServiceProtocol {
    var searchForBookResult: Result<[Book], Error> = .success([])

    func searchForBook(_ bookName: String, completion: @escaping ((Result<[Book], Error>) -> Void)) {
        completion(searchForBookResult)
    }
}
