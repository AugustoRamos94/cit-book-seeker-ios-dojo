//
//  LoadingView.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import UIKit

final class LoadingView: UIView {
    // MARK: Constants

    private struct Constants {
        static let backgroundAlphaColor: CGFloat = 0.4
    }

    // MARK: Properties

    private let activityIndicator: UIActivityIndicatorView = {
        let activityIndicator = UIActivityIndicatorView(style: .large)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        activityIndicator.color = .white
        return activityIndicator
    }()

    // MARK: Constructors

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Activity

    func animate() {
        activityIndicator.startAnimating()
    }

    func stopAnimating() {
        activityIndicator.stopAnimating()
    }

    // MARK: Setup

    private func setupView() {
        backgroundColor = UIColor.black.withAlphaComponent(Constants.backgroundAlphaColor)
        addSubview(activityIndicator)

        NSLayoutConstraint.activate([
            activityIndicator.leadingAnchor.constraint(equalTo: leadingAnchor),
            activityIndicator.trailingAnchor.constraint(equalTo: trailingAnchor),
            activityIndicator.topAnchor.constraint(equalTo: topAnchor),
            activityIndicator.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }
}
