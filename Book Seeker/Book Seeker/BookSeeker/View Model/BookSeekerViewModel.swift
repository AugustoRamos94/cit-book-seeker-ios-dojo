//
//  BookSeekerViewModel.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

final class BookSeekerViewModel {

    // MARK: Properties

    private let bookService: BookSeekerServiceProtocol
    private var books: [Book] = []

    // MARK: Closures

    var onPerformingAsyncRequest: ((Bool) -> Void)?
    var onListHasChanged: (() -> Void)?
    var onRequestError: (() -> Void)?
    var onSelectItem: ((Book) -> Void)?

    // MARK: Init

    init(bookService: BookSeekerServiceProtocol =  BookSeekerService()) {
        self.bookService = bookService
    }

    // MARK: Search

    func searchForBook(_ bookName: String) {
        onPerformingAsyncRequest?(true)
        bookService.searchForBook(bookName) { [weak self] result in
            guard let self = self else { return }
            self.onPerformingAsyncRequest?(false)

            switch result {
            case .success(let list):
                self.books = list
                self.onListHasChanged?()
            case .failure:
                self.onRequestError?()
            }
        }
    }

    // MARK: Items

    func viewDidSelectItem(atIndexPath indexPath: IndexPath) {
        let item = books[indexPath.item]
        onSelectItem?(item)
    }

    func numberOfItemsInSection(_ section: Int) -> Int {
        books.count
    }

    func viewModel(atIndexPath indexPath: IndexPath) -> BookSeekerlCollectionViewModel {
        let model = books[indexPath.item]
        return BookSeekerlCollectionViewModel(model: model)
    }
}
