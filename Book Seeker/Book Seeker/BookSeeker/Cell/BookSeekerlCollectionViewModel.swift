//
//  BookSeekerlCollectionViewModel.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

final class BookSeekerlCollectionViewModel {

    // MARK: Properties

    private let model: Book

    var title: String { model.name }
    var authorName: String { model.authorName }

    // MARK: Init

    init(model: Book) {
        self.model = model
    }
}
