//
//  BookSeekerlCollectionViewCell.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 12/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import UIKit

final class BookSeekerlCollectionViewCell: UICollectionViewCell {

    private struct Constants {
        static let imageViewPlaceholder = UIImage(systemName: "cloud.fill")
        struct borderView {
            static let cornerRadius: CGFloat = 6
            static let borderWidth: CGFloat = 1
            static let borderColor: UIColor = .systemGray
        }
    }

    // MARK: IBOutlets

    @IBOutlet weak private var borderView: UIView!
    @IBOutlet weak private(set) var imageView: UIImageView!
    @IBOutlet weak private(set) var titleLabel: UILabel!
    @IBOutlet weak private(set) var authorLabel: UILabel!

    // MARK: Properties

    var viewModel: BookSeekerlCollectionViewModel? {
        didSet {
            setupUI()
        }
    }

    // MARK: Life cycle

    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = Constants.imageViewPlaceholder
        titleLabel.text = nil
        authorLabel.text = nil
    }

    // MARK: Setup

    private func setupUI() {
        borderView.layer.cornerRadius = Constants.borderView.cornerRadius
        borderView.layer.borderWidth = Constants.borderView.borderWidth
        borderView.layer.borderColor = Constants.borderView.borderColor.cgColor

        titleLabel.text = viewModel?.title
        authorLabel.text = viewModel?.authorName
    }
}
