//
//  BookSeekerCoordinator.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import UIKit

final class BookSeekerCoordinator: Coordinator {

    var navigationController: UINavigationController?

    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }

    func start() {
        let viewModel = BookSeekerViewModel()
        let controller = BookSeekerViewController(viewModel: viewModel)
        navigationController?.pushViewController(controller, animated: true)
    }
}

// MARK: - BookSeekerDelegate

extension BookSeekerCoordinator: BookSeekerDelegate {
    func bookSeeker(_ controller: BookSeekerViewController, didSelectBook: Book) {
        //TODO: handle navigation to details screen
    }
}
