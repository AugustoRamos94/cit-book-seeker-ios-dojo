//
//  BookSeekerAPIResponse.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

struct GenericAPIResponse<T: Decodable>: Decodable {
    let resultCount: Int
    let results: [T]
}

struct Book {
    let trackId: Int
    let name: String
    let description: String
    let authorName: String
    let url: String
    let releaseDate: String
}

// MARK: - Decodable

extension Book: Decodable {
    enum CodingKeys: String, CodingKey {
        case name = "trackName"
        case trackId
        case url = "artworkUrl60"
        case releaseDate
        case description
        case authorName = "artistName"
    }
}
