//
//  BookSeekerViewController.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import UIKit

protocol BookSeekerDelegate: class {
    func bookSeeker(_ controller: BookSeekerViewController, didSelectBook: Book)
}

final class BookSeekerViewController: UIViewController {

    private struct Constants {
        static let throttleTimeInterval: Int = 500
        static let cellWidthMultipler: CGFloat = 0.9
    }

    // MARK: UI Elements

    private lazy var collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        layout.minimumInteritemSpacing = 5
        layout.minimumLineSpacing = 5
        layout.sectionInset = .init(top: 5, left: 5, bottom: 5, right: 5)
        
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        collectionView.backgroundColor = .white
        collectionView.register(UINib(nibName: BookSeekerlCollectionViewCell.identifier, bundle: nil),
                                forCellWithReuseIdentifier: BookSeekerlCollectionViewCell.identifier)
        return collectionView
    }()
    private lazy var searchController: UISearchController = {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.obscuresBackgroundDuringPresentation = true
        searchController.searchBar.placeholder = "Search Books"
        searchController.searchBar.delegate = self
        return searchController
    }()

    private let loadingView: LoadingView = {
        let loadingView = LoadingView()
        loadingView.translatesAutoresizingMaskIntoConstraints = false
        loadingView.isHidden = true
        return loadingView
    }()

    // MARK: Properties

    private let viewModel: BookSeekerViewModel
    private var pendingSearchRequestWorkItem: DispatchWorkItem?
    weak var delegate: BookSeekerDelegate?

    // MARK: Init

    init(viewModel: BookSeekerViewModel, delegate: BookSeekerDelegate? = nil) {
        self.viewModel = viewModel
        self.delegate = delegate
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setpViewModelBinds()
    }

    // MARK: Setup

    private func setupView() {
        title = "Book Seeker"
        navigationItem.searchController = searchController
        view.addSubview(collectionView)
        view.addSubview(loadingView)

        NSLayoutConstraint.activate([
            collectionView.topAnchor.constraint(equalTo: view.topAnchor),
            collectionView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            collectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            collectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor),

            loadingView.topAnchor.constraint(equalTo: view.topAnchor),
            loadingView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            loadingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            loadingView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
    }

    private func setpViewModelBinds() {
        viewModel.onListHasChanged = { [weak self] in
            DispatchQueue.main.async {
                self?.collectionView.reloadData()
            }
        }

        viewModel.onPerformingAsyncRequest = { [weak self] shouldDisplayLoading in
            DispatchQueue.main.async {
                self?.loadingView.isHidden = !shouldDisplayLoading
                shouldDisplayLoading ? self?.loadingView.animate() : self?.loadingView.stopAnimating()
            }
        }

        viewModel.onRequestError = {
            //TODO: HANDLE ERROR
        }

        viewModel.onSelectItem = { [weak self] item in
            guard let self = self else { return }
            self.delegate?.bookSeeker(self, didSelectBook: item)
        }
    }
}

// MARK: - UICollectionViewDelegate

extension BookSeekerViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.viewDidSelectItem(atIndexPath: indexPath)
    }
}

// MARK: - UICollectionViewDelegateFlowLayout

extension BookSeekerViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width * Constants.cellWidthMultipler,
                      height: 110)
    }
}

// MARK: - UICollectionViewDataSource

extension BookSeekerViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfItemsInSection(section)
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BookSeekerlCollectionViewCell.identifier,
                                                            for: indexPath) as? BookSeekerlCollectionViewCell else {
            preconditionFailure("Shouldn't have failed here")
        }
        let cellViewModel = viewModel.viewModel(atIndexPath: indexPath)
        cell.viewModel = cellViewModel
        return cell
    }
}

// MARK: - UISearchBarDelegate

extension BookSeekerViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        pendingSearchRequestWorkItem?.cancel()

        let requestWorkItem = DispatchWorkItem { [weak self] in
            self?.viewModel.searchForBook(searchText)
        }

        pendingSearchRequestWorkItem = requestWorkItem
        DispatchQueue.main.asyncAfter(deadline: .now() + .milliseconds(Constants.throttleTimeInterval),
                                      execute: requestWorkItem)
    }
}
