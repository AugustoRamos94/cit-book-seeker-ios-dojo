//
//  BookSeekerEndpoint.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

enum BookSeekerEndpoint: APIEndpointProtocol {
    case searchBook(bookName: String)

    // MARK: APIEndpointProtocol

    var path: String {
        return "/search"
    }

    var scheme: String {
        return "https"
    }

    var host: String {
        return "itunes.apple.com"
    }

    var queryStringArguments: [QueryStringRequesterArgument] {
        switch self {
        case .searchBook(bookName: let bookName):
            return [
                QueryStringRequesterArgument(key: "term", value: bookName),
                QueryStringRequesterArgument(key: "entity", value: "ibook")
            ]
        }
    }

    var headers: HTTPHeaders? { .default }

    var httpMethod: HTTPMethod { .get }
}
