//
//  BookSeekerService.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

protocol BookSeekerServiceProtocol {
    func searchForBook(_ bookName: String, completion: @escaping ((Result<[Book], Error>) -> Void))
}

struct BookSeekerService: BookSeekerServiceProtocol {
    private let requester: APIRequesterProtocol

    init(requester: APIRequesterProtocol = APIRequester()) {
        self.requester = requester
    }

    func searchForBook(_ bookName: String, completion: @escaping ((Result<[Book], Error>) -> Void)) {
        let endPoint = BookSeekerEndpoint.searchBook(bookName: bookName)
        requester.request(endPoint: endPoint) { response in
            switch response {
            case .success(let data):
                guard let model: GenericAPIResponse<Book> = ApiDataDecoder.decode(data: data) else {
                    return completion(.failure(APIDecoderError.decode))
                }
                completion(.success(model.results))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
}
