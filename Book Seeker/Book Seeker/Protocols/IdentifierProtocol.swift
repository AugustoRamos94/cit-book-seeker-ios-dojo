//
//  IdentifierProtocol.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import UIKit

protocol IdentifierProtocol {
    static var identifier: String { get }
}

extension UIView: IdentifierProtocol {
    static var identifier: String {
        String(describing: Self.self)
    }
}
