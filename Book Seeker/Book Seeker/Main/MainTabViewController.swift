//
//  MainTabViewController.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import UIKit

final class MainTabViewController: UITabBarController {

    // MARK: Properties

    private let bookSeekerCoordinator: Coordinator

    // MARK: Init

    init() {
        bookSeekerCoordinator = Self.createBookSeekerCoordinator()
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        let tabBarList: [UIViewController] = [bookSeekerCoordinator.navigationController!]
        viewControllers = tabBarList

        bookSeekerCoordinator.start()
    }

    // MARK: Controllers creation

    private static func createBookSeekerCoordinator() -> Coordinator {
        let navigationController = UINavigationController()
        navigationController.tabBarItem = UITabBarItem(tabBarSystemItem: .search, tag: 0)
        let coordinator = BookSeekerCoordinator(navigationController: navigationController)
        return coordinator
    }
}
