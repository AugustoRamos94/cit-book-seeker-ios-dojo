//
//  APIEndpointProtocol.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

protocol APIEndpointProtocol {
    var path: String { get }
    var scheme: String { get }
    var host: String { get }
    var headers: HTTPHeaders? { get }
    var httpMethod: HTTPMethod { get }
    var queryStringArguments: [QueryStringRequesterArgument] { get }
}
