//
//  APIRequester.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

protocol APIRequesterProtocol {
    typealias RequestResult = (Result<Data, Error>) -> Void
    func request(endPoint: APIEndpointProtocol, completion: @escaping RequestResult)
}

final class APIRequester: APIRequesterProtocol {
    private let defaultSession = URLSession(configuration: .default)
    private var dataTask: URLSessionDataTask?

    func request(endPoint: APIEndpointProtocol, completion: @escaping RequestResult) {
        dataTask?.cancel()

        guard let url = RequestBuilder.buildRequest(from: endPoint) else {
            completion(.failure(APIRequesterError.invalidURL))
            return
        }

        dataTask = defaultSession.dataTask(with: url) { data, _, error in
            defer { self.dataTask = nil }
            if let error = error {
                completion(.failure(error))
            } else if let data = data {
                completion(.success(data))
            } else {
                completion(.failure(APIRequesterError.unkown))
            }
        }
        dataTask?.resume()
    }
}
