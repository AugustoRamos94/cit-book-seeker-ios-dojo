//
//  RequestBuilder.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

struct RequestBuilder {
    static func buildRequest(from endpoint: APIEndpointProtocol) -> URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = endpoint.scheme
        urlComponents.host = endpoint.host
        urlComponents.path = endpoint.path
        urlComponents.queryItems = endpoint.queryStringArguments.map { URLQueryItem(name: $0.key, value: $0.value) }

        guard let url = urlComponents.url else { return nil }

        var urlRequest = URLRequest(url: url)
        urlRequest.allHTTPHeaderFields = endpoint.headers?.value
        urlRequest.httpMethod = endpoint.httpMethod.rawValue

        return urlRequest
    }
}
