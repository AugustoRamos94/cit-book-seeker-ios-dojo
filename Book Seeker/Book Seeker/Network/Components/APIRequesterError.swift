//
//  APIRequesterError.swift
//  Book Seeker
//
//  Created by Augusto Ramos on 11/07/20.
//  Copyright © 2020 Augusto Ramos. All rights reserved.
//

import Foundation

enum APIRequesterError: String, Error {
    case unkown
    case invalidURL
}
